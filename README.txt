Enables positive and negative tracking of words and phrases.

Check out the issue queue at http://drupal.org/project/issues/statuses_karma

Mathankumar Chermakani (mathankumarc) wrote and maintains this module.
Contact him at http://drupal.org/user/1315174/contact
or visit his website at http://mathankumar.drupalgardens.com/.